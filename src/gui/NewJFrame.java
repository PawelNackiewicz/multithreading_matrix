package gui;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewJFrame extends javax.swing.JFrame {

    DefaultTableModel outputTableModel;

    public NewJFrame() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane3 = new javax.swing.JScrollPane();
        secInputMatrix = new javax.swing.JTable();
        runCalculate = new javax.swing.JButton();
        TimeLabel = new javax.swing.JLabel();
        RandomButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        outputMatrix = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        firstInputMatrix = new javax.swing.JTable();
        runThreads = new javax.swing.JButton();
        timeThreads = new javax.swing.JLabel();
        result = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        secInputMatrix.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                " ", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        secInputMatrix.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                secInputMatrixInputMethodTextChanged(evt);
            }
        });
        secInputMatrix.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                secInputMatrixKeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(secInputMatrix);
        if (secInputMatrix.getColumnModel().getColumnCount() > 0) {
            secInputMatrix.getColumnModel().getColumn(0).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(1).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(2).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(3).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(4).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(5).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(6).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(7).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(8).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(9).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(10).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(11).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(12).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(13).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(14).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(15).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(16).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(17).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(18).setResizable(false);
            secInputMatrix.getColumnModel().getColumn(19).setResizable(false);
        }

        runCalculate.setText("Run");
        runCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runCalculateActionPerformed(evt);
            }
        });

        TimeLabel.setText("Time: ");

        RandomButton.setText("Random()");
        RandomButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RandomButtonActionPerformed(evt);
            }
        });

        outputMatrix.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                " ", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        outputMatrix.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                outputMatrixInputMethodTextChanged(evt);
            }
        });
        outputMatrix.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                outputMatrixKeyTyped(evt);
            }
        });
        jScrollPane4.setViewportView(outputMatrix);
        if (outputMatrix.getColumnModel().getColumnCount() > 0) {
            outputMatrix.getColumnModel().getColumn(0).setResizable(false);
            outputMatrix.getColumnModel().getColumn(1).setResizable(false);
            outputMatrix.getColumnModel().getColumn(2).setResizable(false);
            outputMatrix.getColumnModel().getColumn(3).setResizable(false);
            outputMatrix.getColumnModel().getColumn(4).setResizable(false);
            outputMatrix.getColumnModel().getColumn(5).setResizable(false);
            outputMatrix.getColumnModel().getColumn(6).setResizable(false);
            outputMatrix.getColumnModel().getColumn(7).setResizable(false);
            outputMatrix.getColumnModel().getColumn(8).setResizable(false);
            outputMatrix.getColumnModel().getColumn(9).setResizable(false);
            outputMatrix.getColumnModel().getColumn(10).setResizable(false);
            outputMatrix.getColumnModel().getColumn(11).setResizable(false);
            outputMatrix.getColumnModel().getColumn(12).setResizable(false);
            outputMatrix.getColumnModel().getColumn(13).setResizable(false);
            outputMatrix.getColumnModel().getColumn(14).setResizable(false);
            outputMatrix.getColumnModel().getColumn(15).setResizable(false);
            outputMatrix.getColumnModel().getColumn(16).setResizable(false);
            outputMatrix.getColumnModel().getColumn(17).setResizable(false);
            outputMatrix.getColumnModel().getColumn(18).setResizable(false);
            outputMatrix.getColumnModel().getColumn(19).setResizable(false);
        }

        firstInputMatrix.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                " ", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        firstInputMatrix.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                firstInputMatrixInputMethodTextChanged(evt);
            }
        });
        firstInputMatrix.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                firstInputMatrixKeyTyped(evt);
            }
        });
        jScrollPane5.setViewportView(firstInputMatrix);
        if (firstInputMatrix.getColumnModel().getColumnCount() > 0) {
            firstInputMatrix.getColumnModel().getColumn(0).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(1).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(2).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(3).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(4).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(5).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(6).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(7).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(8).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(9).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(10).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(11).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(12).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(13).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(14).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(15).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(16).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(17).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(18).setResizable(false);
            firstInputMatrix.getColumnModel().getColumn(19).setResizable(false);
        }

        runThreads.setText("Run with Threads");
        runThreads.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runThreadsActionPerformed(evt);
            }
        });

        timeThreads.setText("Time: ");

        result.setText("Result");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RandomButton)
                            .addComponent(timeThreads)
                            .addComponent(TimeLabel)
                            .addComponent(result))
                        .addGap(43, 43, 43)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(121, 121, 121)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 629, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(runCalculate)
                                .addGap(36, 36, 36)
                                .addComponent(runThreads)))))
                .addContainerGap(286, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(RandomButton)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(runCalculate)
                        .addComponent(runThreads)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(TimeLabel)
                        .addGap(36, 36, 36)
                        .addComponent(timeThreads)
                        .addGap(35, 35, 35)
                        .addComponent(result))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void secInputMatrixInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_secInputMatrixInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_secInputMatrixInputMethodTextChanged

    private void secInputMatrixKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_secInputMatrixKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_secInputMatrixKeyTyped

    private void runCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runCalculateActionPerformed
        clearOutputMatrix();
        Integer[][] A = new Integer[20][20];
        A = getValueFromInputMatrix(secInputMatrix);
        Integer[][] C = new Integer[20][20];
        C = getValueFromInputMatrix(firstInputMatrix);
        long elapsedTime;
        long start = System.nanoTime();
        Integer[][] result = null;
        try {
            result = Multiplicar.multiplicar(A, C);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        getResult(result);
        elapsedTime = System.nanoTime() - start;
//        String time = Long.toString(Multiplicar.getElapsedTime());
        TimeLabel.setText("Time : " + elapsedTime);
    }//GEN-LAST:event_runCalculateActionPerformed

    private void RandomButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RandomButtonActionPerformed
        randomValues(firstInputMatrix);
        randomValues(secInputMatrix);
    }//GEN-LAST:event_RandomButtonActionPerformed

    private void outputMatrixInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_outputMatrixInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_outputMatrixInputMethodTextChanged

    private void outputMatrixKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_outputMatrixKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_outputMatrixKeyTyped

    private void firstInputMatrixInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_firstInputMatrixInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_firstInputMatrixInputMethodTextChanged

    private void firstInputMatrixKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_firstInputMatrixKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_firstInputMatrixKeyTyped

    private void runThreadsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runThreadsActionPerformed
        clearOutputMatrix();
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<Integer> future;
        System.out.println(result);
        Integer[][] matrixA = new Integer[20][20];
        matrixA = getValueFromInputMatrix(secInputMatrix);
        Integer[][] matrixB = new Integer[20][20];
        matrixB = getValueFromInputMatrix(firstInputMatrix);
        Integer[][] matrixC = new Integer[20][20];
        long elapsedTime;
        long start = System.nanoTime();
        try {
            for (int rowCount = 0; rowCount < 20; rowCount++) {
                for (int columnCount = 0; columnCount < 20; columnCount++) {
                    MiltiplicationWithThread miltiplicationWithThread = new MiltiplicationWithThread(rowCount, columnCount, matrixA, matrixB);
                    future = executorService.submit(miltiplicationWithThread);
                    matrixC[rowCount][columnCount] = future.get();
//                    System.out.println("result: "+matrixC[rowCount][columnCount]);
                }
            }
        } catch (Exception e) {
            System.out.println("here 2");
            System.out.println(e.toString());
        }
        for (int i = 0; i < matrixC.length; i++) {
            for (int j = 0; j < matrixC.length; j++) {
                System.out.print(matrixC[i][j] + " ");
            }
            System.out.println("");
        }
        try {
            getResult(matrixC);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        elapsedTime = System.nanoTime() - start;
        timeThreads.setText("Time with Threads : " + elapsedTime);
        try {
            String timeWithoutString = TimeLabel.getText().substring(7);
            cechTimeResult(Long.parseLong(timeWithoutString), elapsedTime);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }//GEN-LAST:event_runThreadsActionPerformed

    private void cechTimeResult(long timeWithoutThreads, long timeWithThread) {
        if (timeWithThread >= timeWithoutThreads) {
            result.setText("- " + Long.toString(timeWithThread - timeWithoutThreads));
        } else {
            result.setText("+ " + Long.toString(timeWithoutThreads - timeWithThread));
        }
    }

    public void getResult(Integer[][] result) {
        for (int i = 0; i < 20; i++) {
            outputTableModel.insertRow(outputTableModel.getRowCount(), new Object[]{result[i][0], result[i][1], result[i][2], result[i][3], result[i][4], result[i][5], result[i][6], result[i][7], result[i][8], result[i][9],
                result[i][10], result[i][11], result[i][12], result[i][13], result[i][14], result[i][15], result[i][16], result[i][17], result[i][18], result[i][19]});
        }
    }

    public void clearOutputMatrix() {
        outputTableModel = (DefaultTableModel) outputMatrix.getModel();
        int rowCount = outputTableModel.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            outputTableModel.removeRow(i);
        }
    }

    public Integer[][] getValueFromInputMatrix(JTable inputMatrix) {
        Integer[][] matrix = new Integer[20][20];
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                matrix[i][j] = (Integer) inputMatrix.getModel().getValueAt(i, j);
                //System.out.println("@@ : " + matrix[i][j]);
            }
        }
        return matrix;
    }

    public Integer[][] randomValues(JTable matrixTable) {
        Random random = new Random();
        Integer[][] matrix = new Integer[20][20];
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                matrixTable.getModel().setValueAt((Integer) random.nextInt(10) + 1, i, j);
            }
        }
        return matrix;
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton RandomButton;
    private javax.swing.JLabel TimeLabel;
    private javax.swing.JTable firstInputMatrix;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable outputMatrix;
    private javax.swing.JLabel result;
    private javax.swing.JButton runCalculate;
    private javax.swing.JButton runThreads;
    private javax.swing.JTable secInputMatrix;
    private javax.swing.JLabel timeThreads;
    // End of variables declaration//GEN-END:variables
}
