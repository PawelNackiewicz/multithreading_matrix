package gui;

public class Multiplicar {

    private static long elapsedTime;

    public Multiplicar() {
    }

    public static Integer[][] multiplicar(Integer[][] A, Integer[][] B) throws InterruptedException {

        long start = System.nanoTime();
        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;
        if (aColumns != bRows) {
            throw new IllegalArgumentException("A:Rows: " + aColumns + " did not match B:Columns " + bRows + ".");
        }
        Integer[][] C = new Integer[aRows][bColumns];
        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < bColumns; j++) {
                C[i][j] = 0;
            }
        }
        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < bColumns; j++) { // bColumn
                for (int k = 0; k < aColumns; k++) { // aColumn
//                    Thread.sleep(1);
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        elapsedTime = System.nanoTime() - start;
        return C;
    }
    public static long  getElapsedTime(){
        return elapsedTime;
    }

     
}
